#!/usr/bin/env python3

# Created On: 2018 October 22
# Created By: Pete Maynard

# Description: Run a matrix bot.
import signal
import sys
import os
import logging

# ETA module
from datetime import date
import math

from module.gitlab import *

from matrix_client.client import MatrixClient

rooms_connected = []

def signal_user_1(signum, frame):
	for r in rooms_connected:
		r.send_text(calulate_eta())

def calulate_eta():
	logger.debug("Calling ETA")
	d0 = date(2019, 1, 10)
	d1 = date.today()
	delta = (d0 - d1)
	return "There are {0:.0f} days left ! Which is {1:.0f} weeks or {2} months.".format(delta.days, math.floor(delta.days / 7), math.floor(delta.days / 30))

def on_message(room, event):
	# if event['type'] == "m.room.member":
	# 	if event['membership'] == "join":
	# 		logger.info("{0} joined".format(event['content']['displayname']))

	if event['type'] == "m.room.message":
		if event['content']['msgtype'] == "m.text":

			# Calculate ETA
			if event['content']['body'] == '!eta':
				room.send_text(calulate_eta())

			# Add git lab issues
			if event['content']['body'].startswith('!gitlab add'):
				msg = event['content']['body'].split(' ')
				if(len(msg) == 2):
					room.send_text("e.g. !gitlab add This is the issue title | This is the body of the issue, **Markdown** should work.")
				if(len(msg) > 3 and len(event['content']['body'].split('|')) == 2):
					issue = event['content']['body'].split('|')
					room.send_text(gitlab_create_issue(issue[0][12:], issue[1][1:], event['sender']))
	else:
		logger.info(event['type'])

def main(host, user_id, token, rooms):
	host_url = "https://"+host
	logger.info('Host URL: %s', host_url)
	client = MatrixClient(host_url, user_id=user_id, token=token)
	
	for r in rooms:
		# TODO: Disconnect if it finds another instance of itself.
		room_to_join = "#" + r + ":" + host
		logger.info('Joing Room: %s', room_to_join)
		room = client.join_room(room_to_join)
		room.add_listener(on_message)
		rooms_connected.append(room)
	
	client.start_listener_thread()

	while True:
		pass

if __name__ == '__main__':
	logging.basicConfig(level=logging.INFO)
	logger = logging.getLogger('main')
	signal.signal(signal.SIGUSR1, signal_user_1)
	try:
		with open(".token", 'r') as f:
			token = f.readlines()

	except Exception as e:
		print("Make sure to place a token in '.token'")
		sys.exit()

	with open(".pid", 'w') as f:
		print("{}".format(os.getpid()), end="", file=f)

	# Config Stuffs.
	rooms = ["roomtesting", "hq"]
	main("defendtheweb.co.uk", "@auto-sentinel:defendtheweb.co.uk", token, rooms)


