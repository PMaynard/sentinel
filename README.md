# Sentinel - Matrix Bot

See above.

Trigger routine events:

	kill -s 10 $(cat .pid)

# Development

Configure a virtual environment

    python3 -m venv venv
    . venv/bin/activate

Install dependencies

    pip install matrix_client