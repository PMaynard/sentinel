import logging
import requests

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('gitlab')

def gitlab_create_issue(title, body, user):
	try:
		with open("module/gitlab.conf", 'r') as f:
			project_id = f.readline().strip()
			token = f.readline().strip()
			users = f.readline().split(",")

	except Exception as e:
		print("Make sure you have a 'gitlab.conf'")
		return "This is broken"

	if not user in users:
		return "Not authenticated"
		
	logger.info('create issue : authed user: %s', user)

	payload = {'title': title, 'description': body, 'labels': 'sentinel'}
	r = requests.post("https://gitlab.com/api/v4/projects/"+project_id+"/issues", params=payload, headers={"PRIVATE-TOKEN": token})

	if r.status_code == 201:
		return "Done"

	return "Error: " + str(r.status_code)


